angular.module('pdrmenumod', [])
.directive('pdrMenu', [function() {
  let ddo = {};

  ddo.restrict = 'E';
  ddo.transclude = true;
  ddo.scope = {};

  ddo.template = '<ul ng-transclude></ul>';

  ddo.controller = ['$scope', function($scope) {
  }];

  return ddo;
}])
.directive('pdrItem', [function() {
  let ddo = {};

  ddo.require = '^^pdrMenu';
  ddo.restrict = 'E';
  ddo.transclude = true;

  ddo.scope = {
    title: '@',
    path: '@',
    father: '@'
  }

  ddo.link = function (scope, element, attr) {
    let e;
    // scope.getPath = function() {
    //   if (scope.path) {
    //     return '';
    //   }
    //   return "";
    // }

    if(attr.id) {
      e = element.children();
      e[0].classList.add('main-items');
      scope.opened = false;

      scope.onClick = function() {
        if (scope.opened) {
          scope.opened = false;
          e[0].classList.remove('rotate-item-open');
          e[1].classList.add('collapsed');

        } else {
          scope.opened = true;
          e[0].classList.add('rotate-item-open');
          e[1].classList.remove('collapsed');
        }
      }
    }
  }

  ddo.template = `<li><a ng-click="onClick()" id="a-{{title}}" href="#!/{{path}}">{{title}}</a></li><ul class="sub-menu collapsed" ng-transclude></ul>`;

  return ddo;
}]);
