(function() {

  let diversos = angular.module('fmDiversos', ['fmArray']);

  diversos.factory('IdManager', [function() {

    let manager = {};

    function validateId(id) {
      if (!typeof id == 'number') {
        throw new Error("O id/base deve ser um número válido");
      }
    }
    let Manager = function(id) {
      validateId(id);
      this._base = id;
      this._id = id;
    }
    Manager.prototype.generateId = function() {return ++this._id};
    Manager.prototype.actual = function()  {return this._id};
    Manager.prototype.setNewBase = function(base) {
      if (angular.isDefined(base)) {
        validateId(base);
        this._base = base;
        if (this._id < base) {
          this._id = base;
        }
      }
    }
    Manager.prototype.reset = function() {
      this._id = this._base;
    }

    manager.newManager = (id = 0) => new Manager(id);

    return manager;

  }]);

  diversos.factory('ListenerManager', ['IdManager', 'FMArray', 'Listener', 'normalizeParam', function(IdManager, FMArray, Listener, normalizeParam) {

    let ListenerManager = function() {
      this._ids = IdManager.newManager();
      this._listeners = new FMArray();
    }
    ListenerManager.prototype.addListener = function(listener) {
      listener = normalizeParam(listener);
      let l = new Listener(this._ids.generateId(), listener);
      listener._l = l;
      this._listeners.addData(l);
    }
    ListenerManager.prototype.removeListener = function(listener) {
      listener = normalizeParam(listener);
      if (!listener || !listener._l) {
        throw new Error("O listener passado como parâmetro não é válido ou não foi previamente cadastrado");
      }
      this._listeners.deleteData(listener._l);
    }
    ListenerManager.prototype.notifyAll = function(...data) {
      let ls = this._listeners.getData();
      ls.forEach(l => l.run(data));
    }

    return ListenerManager;

  }]);

  diversos.factory('Listener', [function() {

    function Listener(id, fn) {
      this._id = id;
      if (!angular.isFunction(fn)) {
        throw new Error("Você deve passar uma função válida como parâmetro para o listener");
      }
      this._function = fn;
    }

    let proto = Listener.prototype;

    proto.getId = function() {
      return this_.id;
    }

    proto.run = function(...data) {
      this._function(data);
    }

    return Listener;

  }]);

  diversos.factory('normalizeParam', [function() {
    function normalizeParam(param) {
      if (angular.isArray(param)) {
        return normalizeParam(param[0]);
      }
      return param;
    }
    return normalizeParam;
  }]);

  diversos.factory('presentableDate', [function(){

    function presentableDate(date) {
      if (angular.isDate(date)) {
        return `${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`;
      }
      return date;
    }

    return presentableDate;
  }]);

})();
