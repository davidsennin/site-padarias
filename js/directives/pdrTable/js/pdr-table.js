(function() {

  var pdrTable = angular.module('pdrTable', ['fmArray', 'fmDataServices']);

  pdrTable.directive('pdrTable', ['pdrAttributes', function(pdrAttributes) {
    let ddo = {};

    ddo.restrict = 'A';

    ddo.scope = {
    };

    ddo.controller = ['$scope', function($scope) {
      this.proportions = pdrAttributes.getTableAttrs('proporcoes');
    }];

    ddo.compile = function(tElement, tAttrs) {
      pdrAttributes.setTableAttrs(tAttrs);
      let prop = pdrAttributes.getTableAttrs('proporcoes');
      let proporcoes = prop.split(',');
      pdrAttributes.setTableAttr('proporcoes', proporcoes);

      return {
        pre:
        function(scope, iElement, iAttr) {
        }
      };
    }

    return ddo;
  }]);

  pdrTable.factory('pdrAttributes', [function() {
    let table = {proporcoes: undefined};
    let tableKeys = Object.keys(table);
    let head = {titulos:undefined};
    let headKeys = Object.keys(head);
    let body = {btnDeleta: undefined, btnEdita: undefined, service: undefined};
    let bodyKeys = Object.keys(body);

    function _treatAttrs(tag, attrs, keys) {
      keys.forEach(key => tag[key] = attrs[key]);
    }


    pdrAttributes = {};
    pdrAttributes.setTableAttrs = function(attrs) {
      _treatAttrs(table, attrs, tableKeys);
    }
    pdrAttributes.setTableAttr = function(attr, value) {
      table[attr] = value;
    }
    pdrAttributes.getTableAttrs = function(attr) {
      return table[attr];
    }
    pdrAttributes.setHeadAttrs = function(attrs) {
      _treatAttrs(head, attrs, headKeys);
    }
    pdrAttributes.setHeadAttr = function(attr, value) {
      head[attr] = value;
    }
    pdrAttributes.getHeadAttrs = function(attr) {
      return head[attr];
    }
    pdrAttributes.setBodyAttrs = function(attrs) {
      _treatAttrs(body, attrs, bodyKeys);
    }
    pdrAttributes.setBodyAttr = function(attr, value) {
      body[attr] = value;
    }
    pdrAttributes.getBodyAttrs = function(attr) {
      return body[attr];
    }

    return pdrAttributes;
  }]);

})();
