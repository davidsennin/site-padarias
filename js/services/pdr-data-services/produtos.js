(function() {

  let pdrProdutos = angular.module('pdrDataStore');

  pdrProdutos.factory('ProdutosDataService', ['PdrDataService', 'pdrConfimDeletePanel', 'makeInherit',
  function(PdrDataService, pdrConfimDeletePanel, makeInherit) {

    function ProdutosDataService() {
      PdrDataService.call(this);
    }
    makeInherit(ProdutosDataService, PdrDataService);

    ProdutosDataService.prototype.deleteProduto = function(produto) {
      this.delete(produto).setTipoTexto("Produto");
    }
    ProdutosDataService.prototype.criaProduto = function(produto) {
      produto.custo = parseFloat(produto.custo);
      produto.valor = parseFloat(produto.valor);
      this.addData(produto);
    }
    ProdutosDataService.prototype.getProdutoById = function(id) {
      id = this.cleanParam(id);
      return this.getDataBy("_id", {_id: id});
    }
    ProdutosDataService.prototype.getProdutoByNome = function(_nome) {
      _nome  =this.cleanParam(_nome);
      return this.getDataBy("nome", {nome: _nome});
    }

    return new ProdutosDataService();

  }]);
})();
