(function() {

  let dataSet = angular.module('fmDataSet', ['fmDiversos', 'fmArray']);

  dataSet.factory('FMDataSet', ['FMOrderedArray', 'DataSetListenerManager', 'IdManager', 'normalizeParam', 'objectCopy',
  function(FMOrderedArray, DataSetListenerManager, IdManager, normalizeParam, objectCopy) {
    let FMDataSet = function() {
      this._data = new FMOrderedArray();
      this._listeners = new DataSetListenerManager();
      this._idManager = IdManager.newManager();
    }
    let proto = FMDataSet.prototype;

    proto.addData = function(data) {
      let d0 = objectCopy(data);
      if (this._data.existData(data)) {
        let i = this._data._searchId("", d0);
        this._data.editData(i, d0);
        this._listeners.notifyEditListeners(data);
      } else {
        // Pode ser que já venha com um id definido
        if (!d0._id) {
          d0._id = data._id = this._idManager.generateId();
        }
        this._data.addData(d0);
        this._listeners.notifyAdicionaListeners(data);
      }
    }

    proto.deleteData = function(data) {
      // Faz dessa forma para que todos os listeneres desse evento sejam informados com a informação correta
      data = objectCopy(this.getDataBy("_id", data));
      // A validação (confirmação do usuário) da exclusão não é responsabilidade do data set
      // Porém, validar se o dado foi corretamente excluído é responsabilidade do data set
      if (!this._data.deleteData(data)) {
        throw new Error("Não foi possível realiza a exclusão. Os dados não foram encontrados na lista");
      }
      this._listeners.notifyDeleteListeners(data);
    }

    proto.listeners = function() {
      return this._listeners;
    }

    proto.getData = function() {
      return this._data.getData();
    }

    proto.getDataBy = function(key, value) {
      let k = value;
      if (!angular.isObject(value)) {
        let k = {};
        k[key] = value;
      }
      return this._data.getDataByKey(key, k);
    }

    return FMDataSet;

  }]);

  dataSet.factory('DataSetListenerManager', ['ListenerManager', function(ListenerManager) {

    function DataSetListenerManager() {
      this._addListener = new ListenerManager();
      this._editListener = new ListenerManager();
      this._removeListener = new ListenerManager();
    }

    let _notifyAll = function(list, ...data) {
      list.notifyAll(...data);
    }

    DataSetListenerManager.prototype.addAdicionaListener = function(fn) {
      this._addListener.addListener(fn);
    }

    DataSetListenerManager.prototype.removeAdicionaListener = function(fn) {
      this._addListener.removeListener(fn);
    }

    DataSetListenerManager.prototype.notifyAdicionaListeners = function(...data) {
      _notifyAll(this._addListener, data);
    }

    DataSetListenerManager.prototype.addEditListener = function(fn) {
      this._editListener.addListener(fn);
    }

    DataSetListenerManager.prototype.removeEditListener = function(fn) {
      this._editListener.removeListener(fn);
    }

    DataSetListenerManager.prototype.notifyEditListeners = function(...data) {
      _notifyAll(this._editListener, data);
    }

    DataSetListenerManager.prototype.addDeleteListener = function(fn) {
      this._removeListener.addListener(fn);
    }

    DataSetListenerManager.prototype.removeDeleteListener = function(fn) {
      this._removeListener.removeListener(fn);
    }

    DataSetListenerManager.prototype.notifyDeleteListeners = function(...data) {
      _notifyAll(this._removeListener, data);
    }

    return DataSetListenerManager;

  }]);

})();
