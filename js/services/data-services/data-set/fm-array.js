(function() {

  fmDataSet = angular.module('fmArray', []);

  fmDataSet.factory('FMArray',['objectCopy', function(objectCopy) {

    function getIndexById(id) {
      let index = -1;
      this._list.find((data, i) => {
        if (data._id == id) {
          index = i;
        }
        return data._id == id;
      });
      return index;
    }

    function compareEquals(data0, data1) {
      return data0 == data1;
    }

    function getIndexByKey(key, data) {
      let index = -1;
      this._list.find((arrayData, i) => {
        if (arrayData[key] == data) {
          index = i;
        }
        return arrayData[key] == data;
      });
      return index;
    }

    function editList(index, newData) {
      let obj = this._list[index];
      objectCopy(newData, obj);
    }

    function clearArray(data) {
      if (!angular.isArray(data) || angular.isUndefined(data) || data == null) {
        return [];
      }
      for (var i = data.length; i<=0; i--) {
        if (data[i] == null) {
          data.splice(i, 1);
        }
      }
    }

    let array = function() {
      this._list = [];
    }
    array.prototype = {};
    let arrayPrototype = array.prototype;

    arrayPrototype._searchKey = getIndexByKey;
    arrayPrototype._searchId = getIndexById;
    arrayPrototype._clearArray = clearArray;
    arrayPrototype._maximize = function(key) {
      let value = this._list[0][key];
      let value = this._list.reduce((tot, item) => {
        if (item[key] > tot) {
          return item[key];
        }
        return tot;
      }, value);
      return value;
    }
    // Existe apenas para polimorfismo
    arrayPrototype._getMaxValue = function(key) {
      return this._maximize(key);
    }
    arrayPrototype._put = function(data) {
      // A responsabilidade de verificar a existencia da informação para decidir entre adicionar ou editar
      //ficará no dataset
      this._list.push(data);
    };
    arrayPrototype.editData = editList;

    arrayPrototype.addData = function(data) {
      if (!data._id) {
        throw new Error("o fmArray armazena apenas objetos que possuam o campo _id");
      }
      this._put(data);
    }

    arrayPrototype.assignFromArray = function(dataArray) {
      // this._list = this._list.concat(this._clearArray(dataArray));
      dataArray = this._clearArray(dataArray);
      dataArray.forEach(data => {
        this.addData(data);
      });
    }

    arrayPrototype.deleteData = function(data) {
      let index = this._searchId(data._id);
      if (index == -1) {
        return false;
      }
      this._list.splice(index, 1);
      return true;
    }

    arrayPrototype.compareEquals = compareEquals;

    arrayPrototype.getData = function(i) {
      if (angular.isDefined(i)) {
        return objectCopy(this._list[i]);
      }
      return [].concat(this._list);
    }

    arrayPrototype.existData = function(data) {
      return angular.isDefined(this._list.find((data0) => compareEquals(data0, data)));
    }

    arrayPrototype.getDataByKey = function(key, data) {
      if (!angular.isObject(data)) {
        throw new Error("A informação a ser buscada em getDataByKey deve ser um objeto válido");
      }
      let result = this._list[this._searchKey(key, data[key])];
      if (result) {
        return objectCopy(result);
      }
      return undefined;
    }

    arrayPrototype.getMaxArrayValue = function() {
      return this._getMaxValue("_id");
    }

    return array;

  }]);

  fmDataSet.factory('FMOrderedArray', ['FMArray', 'makeInherit', function(FMArray, makeInherit) {
    // Essa função ainda não trata de valores Object
    let binarySearch = function(key, value, list) {
      if (!list.length) {
        return -1;
      }
      let i = parseInt(list.length/2);
      let v = list[i][key];
      if (value == v) {
        return i;
      }
      if (value < v) {
        return binarySearch(key, value, list.slice(0, i));
      }
      return binarySearch(key, value, list.slice(i+1, list.length));
    }

    function _bis(key, value, list, ini, end) {
      if ((ini > end)||(end < ini)) {
        return ini;
      }
      let i = parseInt((end+ini)/2);
      let v = list[i][key];

      if (value == v) {
        return i;
      }
      if (value < v) {
        return _bis(key, value, list, ini, i-1);
      }
      return _bis(key, value, list, i+1, end);

    }

    let binaryIndexSearch = function(key, value, list) {
      if (!list.length) {
        return -1;
      }
      return _bis(key, value, list, 0, list.length-1);
    }

    let FMOrderedArray = function() {
      FMArray.call(this);
      this._sortKey = "_id";
    }
    makeInherit(FMOrderedArray, FMArray);

    let arrayPrototype = FMOrderedArray.prototype;

    arrayPrototype._searchId = function(data) {
      let value = data[this._sortKey];
      if (!value) {
        value = data;
      }
      return binaryIndexSearch(this._sortKey, value, this._list);
    }

    // arrayPrototype.-searchKey = function(key, data) {
    //   return binarySearch(this._sortKey, data[this._sortKey], this._list);
    // }
    arrayPrototype._getMaxValue = function(key) {
        if (this._sortKey == key) {
          return this._list[this._list.length-1];
        }
        return this._maximize(key);
    }
    arrayPrototype._put = function(data) {
      let index = this._searchId(data);
      this._list.splice(index, 0, data);
    }

    arrayPrototype.existData = function(data) {
      let sk = this._sortKey;
      let d = binarySearch(sk, data[sk], this._list);
      return d != -1;

    }
    return FMOrderedArray;

  }]);

  fmDataSet.factory('objectCopy', [function() {

    function _validateKey(key) {
      if (key.indexOf("$") != -1) {
        return false;
      }
      return true;
    }

    function _duplicate(from) {
      if (!angular.isObject(from) || angular.isDate(from)) {
        return from;
      }
      let obj = {};
      Object.keys(from).forEach((key) => {
        if (_validateKey(key)) {
          obj[key] = _duplicate(from[key]);
        }
      });
      return obj;
    }

    function _run(from, to) {
      if (!to) {
        to = {};
      }
      Object.keys(from).forEach(key => {
        if (_validateKey(key)) {
          to[key] = _duplicate(from[key]);
        }
      });
      return to;
    }

    function objectCopy(from, to) {
      if (!angular.isObject(from)) {
        throw new Error("Impossível executar o objectCopy em tipos não-objetos");
      }
      return _run(from, to);
    }

    return objectCopy;

  }]);

  fmDataSet.factory('makeInherit', [function() {
    function makeInherit(son, dad) {
      var arrPro = Object.create(dad.prototype);
      son.prototype = arrPro;
      son.prototype.constructor = son;
    }
    return makeInherit;
  }]);


})();
