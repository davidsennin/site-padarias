(function() {

  let module = angular.module('pdrDataStore');

  module.factory('PdrLancamentos', ['PdrDataService', 'makeInherit', 'ProdutosDataService', 'presentableDate',
  function(PdrDataService, makeInherit, ProdutosDataService, presentableDate) {

    function PdrLancamentos() {
      PdrDataService.call(this);
    }
    makeInherit(PdrLancamentos, PdrDataService);

    PdrLancamentos.prototype.createData = function(item) {
      let produto = this.getProduto(item.produto);
      let newItem = {};
      newItem._id = item._id;
      newItem.produto_id = produto._id;
      newItem.quantidade = parseInt(item.quantidade);
      newItem.data = item.data;
      return newItem;
    }
    PdrLancamentos.prototype._makeItemToScreen = function(item) {
      let produto = this.getProduto(item.produto_id);
      return {_id:item._id,produto:produto.nome,data:new Date(item.data),quantidade:item.quantidade};
    }
    PdrLancamentos.prototype._makeItemToTable = function(item) {
      let produto = this.getProduto(item.produto_id);
      return {_id:item._id,produto:produto.nome,data:presentableDate(item.data),quantidade:item.quantidade};
    }
    PdrLancamentos.prototype.getProduto = function(data) {
      if (typeof data == 'string') {
        return ProdutosDataService.getProdutoByNome(data);
      } else {
        return ProdutosDataService.getProdutoById(data);
      }
    }

    PdrLancamentos.prototype.createCustomListener = function(fn, listener) {
      let self = this;
      function _l (tableListener) {
        tableListener = self.cleanParam(tableListener);

        function intermission(estoque) {

          function dataParser(estoque) {
            estoque = self.cleanParam(estoque);
            return self._makeItemToTable(estoque);
          }

          data = dataParser(estoque);
          tableListener(data);
        }

        fn.call(listener, intermission);
        tableListener.intermission = intermission;
      }
      return _l;
    }
    PdrLancamentos.prototype.removeCustomListener = function(fn, listener) {
      function _l(tableListener) {
        if (tableListener.intermission) {
          fn.call(listener, tableListener.intermission);
        }
      }
      return _l;
    }
    return PdrLancamentos;

  }]);

})();
