(function() {

  let module = angular.module('padarias');

  module.controller('DesperdiciosController', ['$scope', 'DesperdiciosDataService', 'commonLancamentoController',
  function($scope, DesperdiciosDataService, commonLancamentoController) {

    let superControl = new commonLancamentoController($scope, DesperdiciosDataService, "form_desperdicio");
    $scope.newData = function() {
      $scope.itemDesperdicio = {};
    }
    $scope.newData();

    $scope.receiveEditItem = function(item) {
      $scope.itemDesperdicio = item;
    }

    $scope.create = function() {
      DesperdiciosDataService.criarDesperdicio($scope.itemDesperdicio);
    }

    $scope.addDesperdicio = function() {
      superControl.createData();
    }

    $scope.cancelClick = function() {
      superControl.cancel();
    }

    $scope.setProdutoSearch = function(produto) {
      $scope.itemDesperdicio.produto = produto;
    }

  }]);

})();
