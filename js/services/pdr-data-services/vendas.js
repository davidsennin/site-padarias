(function() {

  let pdrVendas = angular.module('pdrDataStore');

  pdrVendas.factory('VendasDataService', ['PdrLancamentos', 'ProdutosDataService', 'makeInherit', 'presentableDate',
  function(PdrLancamentos, ProdutosDataService, makeInherit, presentableDate) {

    function VendasDataService() {
      PdrLancamentos.call(this);
    }
    makeInherit(VendasDataService, PdrLancamentos);

    VendasDataService.prototype.deleteSale = function(sale) {
      this.delete(sale).setTipoTexto("Venda");
    }
    VendasDataService.prototype.criaVenda = function(venda) {
      let newData = this.createData(venda);
      newData.hora = venda.hora;
      this.addData(newData);
    }
    VendasDataService.prototype._makeItemToScreen = function(item) {
      let produto = this.getProduto(item.produto_id);
      return {_id:item._id,produto:produto.nome,data:new Date(item.data),hora:item.hora,quantidade:item.quantidade};
    }
    VendasDataService.prototype._makeItemToTable = function(item) {
      let produto = this.getProduto(item.produto_id);
      let a = {_id:item._id,produto:produto.nome,data:presentableDate(item.data),hora:item.hora,quantidade:item.quantidade};
      return a;
    }
    VendasDataService.prototype.getVendas = function() {
      return this.getData();
    }
    VendasDataService.prototype.getVendasFull = function() {
      return this.getFullData();
    }
    return new VendasDataService();
  }]);

})();
