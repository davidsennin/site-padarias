(function() {

  var pdrDirectives = angular.module('pdrdirectives', []);

  pdrDirectives.directive('padariaMenu', function(){
    var ddo = {};
    ddo.restrict = 'E';

    ddo.templateUrl = 'js/directives/padaria.html';

    return ddo;
  });

  pdrDirectives.factory("focusManager", [function() {
    let manager = {};

    let listeners = {};
    manager.putListener = function(value, element) {
      listeners[value] = element
    }
    manager.clear = function() {
      listeners = {};
    }
    manager.hear = function(value) {
      let element = listeners[value];
      if (element) {
        element[0].focus();
      }
    }

    return manager;
  }]);

  pdrDirectives.directive('pdrFocus', ['focusManager', function(focusManager) {
    var ddo = {};

    ddo.restrict = 'A';
    ddo.require = '^ngModel';

    ddo.link = function(scope, iElement, iAttr) {
      value = iAttr.pdrFocus;
      focusManager.putListener(value, iElement);
      if (value) {
        scope.$on('padaria', function(event, type) {
          focusManager.hear(type);
        });
        scope.$on('destroy', function() {
          focusManager.clear();
        });
      }
    }

    return ddo;
  }]);

  pdrDirectives.directive('productFieldValidator', ['DataProviderService', function(DataProviderService) {

    let validator = {};

    validator.require = '^ngModel';
    validator.restrict = 'A';

    validator.compile = function(tElement, tAttr) {

      return {
        pre: function(scope, iElement, iAttr, ngModelCtrl) {
        },
        post: function(scope, iElement, iAttr, ngModelCtrl) {
          let service = DataProviderService.getService("Produtos");

          ngModelCtrl.$validators.product = function(modelValue, viewValue) {
            if (ngModelCtrl.$isEmpty(modelValue)) {
              return true;
            }

            let value = viewValue;
            let data = service.safeRun("getByName", value);
            if (data) {
              return true;
            }

            return false;
          }
        }
      }

    }

    return validator;

  }]);

})();
