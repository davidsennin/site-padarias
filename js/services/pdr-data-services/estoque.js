(function() {

  let pdrEstoque = angular.module('pdrDataStore');

  pdrEstoque.factory('EstoqueDataService', ['PdrLancamentos', 'presentableDate', 'makeInherit', 'ProdutosDataService',
  function(PdrLancamentos, presentableDate, makeInherit, ProdutosDataService) {
    function EstoqueDataService() {
      PdrLancamentos.call(this);
    }

    makeInherit(EstoqueDataService, PdrLancamentos);

    EstoqueDataService.prototype.deleteItem = function(item) {
      this.delete(item).setTipoTexto("Item");
    }
    EstoqueDataService.prototype.criaItem = function(item) {
      //Criar validação de produtos no controller
      let newData = this.createData(item);
      this.addData(newData);
    }
    EstoqueDataService.prototype.getEstoque = function() {
      return this.getData();
    }
    EstoqueDataService.prototype.getEstoqueFull = function() {
      return this.getFullData();
    }
    return new EstoqueDataService();

  }]);
})();
