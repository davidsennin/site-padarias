angular.module('padarias', ['ngRoute', 'pdrdirectives', 'pdrmenumod', 'pdrTable', 'pdrPanels',
'ngAnimate', 'fmDiversos', 'pdrDataServices', 'pdrDataStore', 'fmMasks', 'fmSearchPanel', 'pdrRelatorios'])
.config(['$routeProvider', function($routeProvider) {

  $routeProvider.when('/produtos', {
    templateUrl: '/partials/produtos.html',
    controller: 'ProdutosController'
  });

  $routeProvider.when('/estoque', {
    templateUrl: '/partials/lancamento-estoque.html',
    controller: 'LancamentosEstoqueController'
  });

  $routeProvider.when('/itens-estoque', {
    templateUrl: '/partials/itens-estoque.html',
    controller: 'ItensEstoqueController'
  });

  $routeProvider.when('/vendas', {
    templateUrl: '/partials/lancamentos-vendas.html',
    controller: 'VendasController'
  });

  $routeProvider.when('/vendas/relatorio/:tipoRelatorio', {
    templateUrl: '/partials/relatorio.html',
    controller: 'RelatorioController'
  });

  $routeProvider.when('/desperdicio', {
    templateUrl: '/partials/lancamentos-desperdicios.html',
    controller: 'DesperdiciosController'
  });

  $routeProvider.when('/desperdicio/relatorio/:tipoRelatorio', {
    templateUrl: '/partials/relatorio.html',
    controller: 'RelatorioController'
  });

  $routeProvider.otherwise({
    redirectTo: '/produtos'
  });

}]);
