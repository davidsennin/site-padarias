(function() {

  let pdrDataService = angular.module('pdrDataStore', ['fmDataServices', 'fmDataSet']);

  pdrDataService.factory('PdrDataService', ['FMDataSet', 'normalizeParam', 'Listener', 'objectCopy', 'pdrConfimDeletePanel',
  function(FMDataSet, normalizeParam, Listener, objectCopy, pdrConfimDeletePanel) {

    function DataService() {
      this._dataSet = new FMDataSet();
      this._editSetListener = undefined;
    }
    DataService.prototype.deleteData = function(data) {
      this._dataSet.deleteData(data);
    }
    DataService.prototype.setDataToEdit = function(data) {
      data = normalizeParam(data);
      // Como estamos usando um OrderedArray, a key passada não é importante
      //Pois o array usará a key que está sendo usada para ordenação do elementos
      // Só que não mais. As classes de array serão revisadas no futuro (eu espero...)
      let d0 = this.getDataBy("_id", data);
      if (d0) {
        if (this._editSetListener) {
          d0 = objectCopy(d0);
          d0 = this._makeItemToScreen(d0);
          this._editSetListener.run(d0);
        }
      } else {
        throw new Error("A informação passada não foi encontrada na lista");
      }
    }
    DataService.prototype._makeItemToScreen = function(data) {
      return data;
    }
    DataService.prototype.getData = function() {
      self = this;
      let data = this._dataSet.getData();
      let result = [];
      data.forEach((d) => result.push(self._makeItemToTable(d)));
      return result;
    }
    DataService.prototype.getFullData = function() {
      return this._dataSet.getData();
    }
    DataService.prototype.getDataBy = function(key, value) {
      return this._dataSet.getDataBy(key, value);
    }
    DataService.prototype.delete = function(data, confirm, cancel) {
      self = this;
      data = self.cleanParam(data);
      return pdrConfimDeletePanel.openConfirmCancelPanel(
        function() {
          if (confirm) {
            confirm(data);
          }
          self._deleteData(data);
        }, function(){
          if (cancel) {
            cancel(data);
          }
        }).setItem(data);
    }
    DataService.prototype._deleteData = function(dataParam) {
      this._dataSet.deleteData(dataParam);
    }
    DataService.prototype.addData = function(data) {
      this._dataSet.addData(data);
    }
    DataService.prototype.listeners = function() {
      return this._dataSet.listeners();
    }
    DataService.prototype.putEditSetListener = function(fn) {
      this._editSetListener = new Listener(1, fn);
    }

    DataService.prototype.clearEditSetListener = function() {
      this._editListener = undefined;
    }
    DataService.prototype._makeItemToTable = function(data) {
      return data;
    }
    DataService.prototype.cleanParam = function(param) {
      return normalizeParam(param);
    }

    return DataService;

  }]);

})();
