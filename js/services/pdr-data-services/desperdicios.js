(function() {

  let pdrDesperdicios = angular.module('pdrDataStore');

  pdrDesperdicios.factory('DesperdiciosDataService', ['PdrLancamentos', 'ProdutosDataService', 'makeInherit', 'presentableDate',
  function(PdrLancamentos, ProdutosDataService, makeInherit, presentableDate) {

    function DesperdiciosDataService() {
      PdrLancamentos.call(this);
    }
    makeInherit(DesperdiciosDataService, PdrLancamentos);

    DesperdiciosDataService.prototype.deleteWaste = function(waste) {
      this.delete(waste).setTipoTexto("Desperdicios");
    }
    DesperdiciosDataService.prototype.criarDesperdicio = function(desperdicio) {
      let newData = this.createData(desperdicio);
      this.addData(newData);
    }
    DesperdiciosDataService.prototype.getDesperdicios = function() {
      return this.getData();
    }
    DesperdiciosDataService.prototype.getDesperdiciosFull = function() {
      return this.getFullData();
    }
    return new DesperdiciosDataService();



  }]);

})();
