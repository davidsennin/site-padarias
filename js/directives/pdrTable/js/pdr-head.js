(function() {

  var pdrTable = angular.module('pdrTable');

  pdrTable.directive('pdrHeader', ['pdrAttributes', 'TableDimensionService', 'createTableHead',
  function(pdrAttributes, TableDimensionService, createTableHead) {
    let header = {};

    header.restrict = 'A';

    header.scope = {
      titulos: '@'
    };

    header.compile = function(tElement, tAttrs) {
      pdrAttributes.setHeadAttrs(tAttrs);

      let proportions = pdrAttributes.getTableAttrs('proporcoes');

      return {
        pre: function(scope, iElement, iAttr) {
          let t = scope.titulos;
          let titulos = t.split(',');
          TableDimensionService.setProportion(proportions);
          TableDimensionService.setQtdColumns(titulos.length);
          let sizes = TableDimensionService.calculate();

          let newElement = createTableHead(titulos, sizes, scope);
          iElement.append(newElement);
        },
        post: function(scope, iElement, iAttr) {
        }
      };
    }

    return header;
  }]);

  pdrTable.factory('createTableHead', ['$compile', function($compile) {

    function _style(size) {
      return `width:${size}%`;
    }

    function _text(titles, proportions) {
      let text = `<tr>`;
      titles.forEach((item, index) => {
        text += `<th style="${_style(proportions[index])}">${item}</th>`;
      });
      text += `</td>`;
      return text;
    }

    function _compile(text) {
      let element = angular.element(text);
      return $compile(element);
    }

    function createTableHead(titles, proportions, scope) {
      let text = _text(titles, proportions);
      let compiled = _compile(text);
      return compiled(scope);
    }

    return createTableHead;

  }]);

  pdrTable.factory('TableDimensionService', [function() {

    let prop = [];
    let sizes = [];
    let qtdColumns = 0;
    let edit = false;
    let tDelete = false;
    let tableSize = 100;

    function calculateProportions() {
      while(prop.length > qtdColumns) {
        prop.pop();
      }
      while(prop.length < qtdColumns) {
        prop.push(1);
      }
      let decProp = prop.reduce((tot, value) => parseInt(tot)+parseInt(value), 0);
      return decProp;
    }

    function calculateTableMaxSize() {
      let s = tableSize;
      if (edit) {
        s -= 4.5;
      }
      if (tDelete) {
        s -= 4.5;
      }
      return s;
    }

    function calculateRealProportions(s, decProp) {
      let realProportions = prop.map(value => (value/decProp)*s);
      return realProportions;
    }

    let fac = {};

    fac.setProportion = function(proportions) {
      prop = [].concat(proportions);
    }

    fac.setQtdColumns = function(qtd) {
      qtdColumns = qtd;
    }

    fac.setEdit = function() {
      edit = true;
    }

    fac.setDelete = function() {
      tDelete = true;
    }

    fac.calculate = function() {
      return calculateRealProportions(calculateTableMaxSize(), calculateProportions());
    }

    return fac;
  }]);


})();
