(function() {

  let padarias = angular.module('padarias');

  padarias.controller('RelatorioController', ['$scope', '$routeParams', function($scope, $routeParams) {

    $scope.filter = {produto:undefined};
    $scope.service = "";
    $scope.tableTitles = "Data,Produto,Quantidade,Custo,Ganho";

    let servico = $routeParams.tipoRelatorio;
    if (servico) {
      $scope.service = servico;
    }
    if ($scope.service == "RelatorioVenda") {
      $scope.reportTitle = "Relatório de vendas";
    } else if ($scope.service == "RelatorioDesperdiciosQuantidade") {
      $scope.reportTitle = "Relatório de desperdício por quantidade";
    } else if ($scope.service == "RelatorioDesperdiciosTaxa") {
      $scope.reportTitle = "Relatório de desperdício por taxa";
      $scope.tableTitles = "Data,Produto,Quantidade,Custo,Ganho";
    }


  }]);

})();
