(function() {

  let pdrEntries = angular.module('pdrDataServices');

  pdrEntrie.factory('DataHistory', ['DataProviderService', 'FMDataSet', function(DataProviderService, FMDataSet) {

    class DataHistory {

      constructor() {
        this._dataSet = new FMDataSet();
        _initialize();
      }

      _initialize() {
        _initializeDataSets();
        _initializeProductListeners();
      }

      _initializeDataSets() {
        this._produtos = DataProviderService.getService("Produtos");
        this._estoque = DataProviderService.getService("Estoque");
        this._venda = DataProviderService.getService("Venda");
        this._desperdicio = DataProviderService.getService("Desperdicio");
      }

      // Precisamos saber quando um produto é excluído, para que possamos removê-lo do histórico
      // Dessa forma, também podemos notificar os demais data-sets para excluir os lançamentos desse produto
      _initializeProductListeners() {
        let p = this._produtos;
        p.safeRun("removeAddListener");
      }



    }

  }]);

})();
