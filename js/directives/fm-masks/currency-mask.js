(function() {

  let fmMasks = angular.module('fmMasks');

  fmMasks.directive('fmCurrency', ['numberFilter', function(numberFilter) {

    let currency = {};

    currency.restrict = 'A';
    currency.require = '^ngModel';
    currency.scope = {
      data: '='
    };

    let values = {};
    currency.compile = function(tElement, tAttrs) {
      return {
        pre: function(scope, iElement, iAttrs) {
        },
        post: function(scope, iElement, iAttrs, parentCtrl) {

          function findDot(value) {
            return value.indexOf('.');
          }

          function charAfter(inicialPos, value) {
            return (value.length - inicialPos)-1;
          }

          function shiftLeft(value, dotPos) {
            let newValue = value.substring(0, dotPos + 2).replace(".", "") + "." + value.substring(dotPos + 2);
            if (newValue.charAt(0) == '0') {
              newValue = newValue.substring(1);
            }
            return newValue;
          }

          function shiftRight(value, dotPos) {
            let newValue = value.substring(0, dotPos - 1) + "." + value.substring(dotPos -1).replace(".", "");
            if (newValue.charAt(0) == '.') {
              newValue = "0" + newValue;
            }
            return newValue;
          }

          function shift(charAfterDot, value) {
            if (charAfterDot > 2) {
              return shiftLeft(value, findDot(value));
            }
            return shiftRight(value, findDot(value));
          }

          function transform(value) {
            // Sempre deverá haver 2 caracteres após o '.'
            // Caso tenha 1, significa que houve exclusão
            // Caso tenha 3, houve inserção
            let charAfterDot = charAfter(findDot(value), value);
            if (charAfterDot == 2) {
              // Se houverem 2 caracteres, é pq ta tudo maravilhoso
              return value;
            }
            value = shift(charAfterDot, value);

            return value;
          }

          function removeMoneyMark(value) {
            return value.replace('R$ ', '');
          }

          function insertMoneyMark(value) {
            return 'R$ ' + removeMoneyMark(value);
          }

          function parser(value) {
            if (!value) {
              return value;
            }
            // value = value.replace('R$', '');
            value = removeMoneyMark(value);
            value = transform(value);
            // Por algum motivo, quando exclui caracteres ele não chama os parsas
            // Por causa disso, faço essa verificação pra garantir que sempre terá 0.00
            // nos casos onde o valor informado for esse
            if (value == '0.0') {
              value = '0.00';
            }
            parentCtrl.$viewValue = insertMoneyMark(value);
            parentCtrl.$render();
            return value;
          }

          function preFormat(value) {
            if (typeof value == "number") {
              return numberFilter(value, 2);
            }
            return value;
          }

          function formatter(value) {
            if (!value) {
              return value;
            }

            value = removeMoneyMark(preFormat(value));
            value = transform(value);
            parentCtrl.$modelValue = value;
            return insertMoneyMark(value);
          }

          parentCtrl.$parsers.push(parser);
          parentCtrl.$formatters.push(formatter);

          iElement.on('keypress', function(event) {
            if (Number.isNaN(parseInt(event.key))) {
              event.preventDefault();
            }
          });
          iElement.on('focus', function(event) {
            if (this.setSelectionRange) {
              let length = this.value.length;
              this.setSelectionRange(length, length);
            }
          });

          // Essa função só existe por causa da tosqueira que
          //parseFloat("0.00 ajdhaskjdhaksjdhksaj") retorna 0 e não NaN
          function isTrueFloat(value) {
            for (let i = 0; i < value.length; i++) {
              if (value.charAt(i) == '.') {
                continue;
              }
              if (Number.isNaN(parseFloat(value.charAt(i)))) {
                return false;
              }
            }
            return true;
          }

          scope.data = "0.00";
          scope.$watch('data', function(newV, old) {
            if (!newV || !isTrueFloat(newV)) {
              scope.data = "0.00";
            }
          });
        }
      };
    }

    return currency;

  }]);

})();
