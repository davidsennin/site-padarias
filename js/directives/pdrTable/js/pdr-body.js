(function() {

  var pdrTable = angular.module('pdrTable');

  pdrTable.directive('pdrBody', ['pdrAttributes', 'bindBodyScope', 'bodyContentCreate', 'selectedItems',
  function(pdrAttributes, bindBodyScope, bodyContentCreate, selectedItems) {

    let body = {};

    body.restrict = 'A';
    body.scope = {
      filter: "=",
      service: "@"
    };

    body.compile = function(tElement, tAttrs) {
      pdrAttributes.setBodyAttrs(tAttrs);
      let btnEdit = pdrAttributes.getBodyAttrs('btnEdita') == "true";
      let btnDeleta = pdrAttributes.getBodyAttrs('btnDeleta') == "true";
      
      //Não me lembro pq faço isso aqui. Acho que fazia as lógicas antes do link
      let dataService = pdrAttributes.getBodyAttrs('service');

      return {
        pre: function(scope, iElement, iAttr) {
          scope.data = [];
          if (scope.service) {
            dataService = scope.service;
          }
          bindBodyScope(scope, dataService);

          let singleSelect = iAttr['singleSelect'] != undefined;
          let multiSelect = iAttr['multiSelect'] != undefined;
          let noSelect = (iAttr['noSelect'] != undefined) || (singleSelect == true && multiSelect == true);
          scope.noSelect = noSelect;

          if (!noSelect) {
            // Ajustar esse multi-selected (primeiro parâmetro)
            selectedItems.initialize(multiSelect, scope);
          }
        },
        post: function(scope, iElement, iAttr) {
          element = bodyContentCreate(btnEdit, btnDeleta, scope);
          iElement.append(element);
        }
      };
    }

    return body;

  }]);

  pdrTable.factory('bindBodyScope', ['DataProviderService', 'normalizeParam', 'objectCopy',
  function(DataProviderService, normalizeParam, objectCopy) {

    function _bindFunctions(scope) {
      function getIndex(id) {
        let i = -1;
        scope.data.find((arrData, index) => {
          if (arrData._id == id) {
            i = index;
          }
          return arrData._id == id;
        });
        return i;
      }

      scope.show = key => key!="_id";

      scope.adicionaListener = function adicionaListener(data) {
        data = normalizeParam(data);
        scope.data.push(data);
      }

      scope.editListener = function editListener(data) {
        data = normalizeParam(data);
        let oldDataIndex = getIndex(data._id);
        let oldData = scope.data[oldDataIndex];
        objectCopy(data, oldData);
      }

      scope.deleteListener = function (data) {
        data = normalizeParam(data);
        let i = getIndex(data._id);
        scope.data.splice(i, 1);
      }
    }

    function bindScope(scope, dataService) {
      _bindFunctions(scope);
      if (dataService) {
        let k = dataService.safeRun("getData");
        scope.data = scope.data.concat(k);
        dataService.safeRun("addListener", scope.adicionaListener);
        dataService.safeRun("editListener", scope.editListener);
        dataService.safeRun("deleteListener", scope.deleteListener);
        scope.$on('$destroy', function() {
          dataService.safeRun("removeAddListener", scope.adicionaListener);
          dataService.safeRun("removeEditListener", scope.editListener);
          dataService.safeRun("removeDeleteListener", scope.deleteListener);
        });

        scope.editElement = data => {
          dataService.safeRun("setEditData", objectCopy(data));
        }
        scope.deleteElement = data => {
          dataService.safeRun("deleteData", data);
        }
      } else {
        scope.editElement = data => {
          console.log("DataService não foi encontrado");
        }
        scope.deleteElement = data => {
          console.log("DataService não foi encontrado");
        }
      }
    }

    function bindBodyScope(scope, dataService) {
      if (dataService) {
        bindScope(scope, DataProviderService.getService(dataService));
      }
    }

    return bindBodyScope;

  }]);

  pdrTable.factory('bodyContentCreate', ['$compile', function($compile) {
    function createBtn(name, action, bClass) {
      return `
        <td class="button-cell">
          <a class="${bClass} hint" data-hint="${name}" ng-click="${action}">${name}</a>
        </td>
      `;
    }

    function createBtnEdit(btnEdit) {
      let text = ``;
      if (btnEdit === true) {
        text += createBtn("Editar", "editElement(d)", "edit");
      }
      return text;
    }

    function createBtnDelete(btnDelete) {
      let text = ``;
      if (btnDelete === true) {
        text += createBtn("Excluir", "deleteElement(d)", "delete");
      }
      return text;
    }

    function createContent(btnEdit, btnDelete) {
      let text = `
        <tr class="mark" ng-repeat="d in data | filter: filter" pdr-selectable ng-click="onClick(d._id)">
          <td ng-show="show(key)" ng-repeat="(key, value) in d">{{value}}</td>
      `;
      text += createBtnEdit(btnEdit);
      text += createBtnDelete(btnDelete);
      text += `</tr>`;
      return text;
    }

    function _compile(text) {
      let element = angular.element(text);
      return $compile(element);
    }

    function bodyContentCreate(btnEdit, btnDelete, scope) {
      let text = createContent(btnEdit, btnDelete);
      let compiled = _compile(text);
      return compiled(scope);
    }

    return bodyContentCreate;
  }]);

  pdrTable.directive('pdrSelectable', ['selectedItems', function(selectedItems){

    let selectable = {};
    selectable.restrict = 'A';

    let selectableOn = true;

    selectable.compile = function(tElement, tAttr) {
      return {
        pre: function(scope, iElement, iAttr, parentCtrl) {
          // A linha de baixo deve ser descomentada para que seja possível configurar o elemento como selecionávl
          selectableOn = !scope.$parent.noSelect;

          function _unSelect() {
            iElement.css('background-color', '');
          }
          function _select() {
            iElement.css('background-color', 'rgba(100, 100, 200, 0.5)');
          }

          if (selectableOn) {
            scope.onClick = function() {
              selectedItems.putItem(scope.d._id);
            }
            scope.$on('pdrUnselect', function(event, dataId) {
              if (scope.d._id == dataId) {
                _unSelect();
              }
            });
            scope.$on('pdrSelect', function(event, dataId) {
              if (scope.d._id == dataId) {
                _select();
              }
            });
          }

        }
      }
    }

    return selectable;

  }]);

  pdrTable.factory('selectedItems', [function() {

    let items = {};
    let oldId = undefined;//Será usado em casos de single select
    let multiSelected = false;
    let scope = undefined;

    let selectedItems = {};

    function _iterateKeys(fn) {
      let keys = Object.keys(items);
      keys.forEach(key => {fn(key);});
    }

    function _putMultiSelect(item) {
      items[item] = !items[item];
    }

    function _broadcast(event, item) {
      if (scope) {
        scope.$broadcast(event, item);
      }
    }

    function _putSingleSelect(item) {
      if (oldId) {
        items[oldId] = false;
        _broadcast('pdrUnselect', oldId);
      }
      if (oldId == item) {
        oldId = undefined;
      } else {
        items[item] = true;
        oldId = item;
        _broadcast('pdrSelect', oldId);
      }
    }

    function _parseKey(key) {
      if (typeof key == "string") {
        let a = parseInt(key);
        if (Number.isNaN(a)) {
          a = parseFloat(key);
          if (Number.isNaN(a)) {
            return key;
          }
          return a;
        }
        return a;
      } else {
        return key;
      }
    }

    selectedItems.initialize = function(multi, $scope) {
      multiSelected = multi;
      scope = $scope;
    }

    // Esse escopo servirá para notificar o antigo item selecionado que ele não é mais o chosen one
    selectedItems.putItem = function(item) {
      if (multiSelected) {
        _putMultiSelect(item);
      } else {
        _putSingleSelect(item);
      }
    }

    selectedItems.getSelected = function() {
      let result = [];
      _iterateKeys(function(key) {
        if (items[key]) {
          result.push(_parseKey(key));
        }
      });
      return result;
    }

    return selectedItems;

  }]);

})();
