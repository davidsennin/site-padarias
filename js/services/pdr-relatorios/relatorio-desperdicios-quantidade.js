(function() {

  let pdrRelatorios = angular.module('pdrRelatorios');

  pdrRelatorios.factory('RelatorioDesperdiciosQuantidade', ['numberFilter', 'PdrRelatorios',
  function(numberFilter, PdrRelatorios) {

    class RelatorioDesperdicios extends PdrRelatorios {

      constructor() {
        super("Desperdicio", "getFullData");
      }

      _createService(provider) {
        provider.putNewService("RelatorioDesperdiciosQuantidade");
        provider.putFunctionForService("RelatorioDesperdiciosQuantidade", "getData", this.getData, this);
      }

    }

    return new RelatorioDesperdicios();

  }]);

})();
