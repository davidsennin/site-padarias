(function() {

  let pdrRelatorios = angular.module('pdrRelatorios', []);

  pdrRelatorios.factory('PdrRelatorios', ['DataProviderService', 'presentableDate', 'numberFilter',
  function(DataProviderService, presentableDate, numberFilter) {

    class PdrRelatorios {
      constructor(reportServiceName, reportServiceDataFunction) {
        let self = this;
        self._dataFunctionName = reportServiceDataFunction;
        self._serviceName = reportServiceName;
        
        DataProviderService.getServicePromisse(reportServiceName).then(function(service) {
          self._reportService = service;
          self._createService(DataProviderService);
        }, function() {
          console.log("Erro ao tentar receber o serviço de Dados");
        });

        DataProviderService.getServicePromisse("Produtos").then(function(service) {
          console.log(service);
          self._produtos = service;
          console.log(self);
        }, function() {
          console.log("Erro ao tentar receber o serviço de Produtos");
        });
      }



      _createService(provider) {
        // DataProviderService.putNewService("RelatorioVenda");
        // DataProviderService.putFunctionForService("RelatorioVenda", "getData", this.getData, this);
      }


      _validateServices() {
        if (!this._reportService) {
          throw new Error("Não é possível criar relatórios sem ter acesso a uma classe de dados cadastrada em DataProviderService");
        }
        if (!this._produtos) {
          throw new Error("Não é possível criar relatórios de dados sem ter uma classe de produtos cadastrada em DataProviderService");
        }
      }

      _getData() {
        let data = this._reportService.safeRun(this._dataFunctionName);
        if (!data) {
          throw new Error("Ocorreu algum erro ao buscar os dados das vendas");
        }
        return data;
      }

      _getProduto(prod_id) {
        let produto = this._produtos.safeRun("getById", prod_id);
        if (!produto) {
          throw new Error(`Ocorreu algum erro ao buscar os dados do produto ${prod_id}`);
        }
        return produto;
      }

      _newDate(data, produto, dado) {
        let t = {};
        t.data = data;
        t.produto = produto.nome;
        t.quantidade = dado.quantidade;
        t.custo = numberFilter(produto.custo * dado.quantidade, 2);
        t.valor = numberFilter(produto.valor * dado.quantidade, 2);
        return t;
      }

      _refreshData(dataTotalizada, dado, produto) {
        dataTotalizada.quantidade += dado.quantidade;
        dataTotalizada.custo = numberFilter(produto.custo * dataTotalizada.quantidade, 2);
        dataTotalizada.valor = numberFilter(produto.valor * dataTotalizada.quantidade, 2);
      }


        // _newDate(data, produto, dado){}
        //
        // _refreshData(dataTotalizada, dado, produto){}

        _totalizaData(data, totProd, dadoTotalizar, produto) {
          let date = totProd[data];
          if (date) {
            this._refreshData(date, dadoTotalizar, produto);
          } else {
            totProd[data] = this._newDate(data, produto, dadoTotalizar);
          }
        }

        _totaliza(dadoTotalizar, dados) {
          let prod_id = dadoTotalizar.produto_id;
          let produto = this._getProduto(prod_id);
          let p2 = dados[produto.nome];
          if (p2) {
            this._totalizaData(presentableDate(dadoTotalizar.data), p2, dadoTotalizar, produto);
          } else {
            dados[produto.nome] = {};
            p2 = dados[produto.nome];
            this._totalizaData(presentableDate(dadoTotalizar.data), p2, dadoTotalizar, produto);
            console.log(p2);
          }
        }

        _organizeDatesToArray(produto, result) {
          let datas = Object.keys(produto);
          datas.forEach((data) => {
            console.log(produto[data][0]);
            let item = produto[data];
            result.push(item);
          });
        }

        _organizeProductsToArray(dados) {
          let  result = [];
          console.log(dados);
          let keys = Object.keys(dados);
          keys.forEach((key) => {
            console.log(dados[key]);
            this._organizeDatesToArray(dados[key], result);
          });
          console.log(result);
          return result;
        }

        getData() {
          this._validateServices();
          let totalizado = {};
          let dados = this._getData();
          dados.forEach((dadoTotalizar) => {
            this._totaliza(dadoTotalizar, totalizado)
          });
          let result = this._organizeProductsToArray(totalizado);
          return result;
        }

    }

    return PdrRelatorios;
  }]);

})();
