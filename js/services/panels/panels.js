(function() {
  let pdrPanels = angular.module('pdrPanels', []);

  pdrPanels.service('pdrConfimDeletePanel', ['$document', '$templateCache', '$rootScope', '$compile', '$animate', '$injector',
  function($document, $templateCache, $rootScope, $compile, $animate, $injector) {

    let body = $document.find('body');

    function CommonControl($scope, confirmCallback, cancelCallback) {
      $scope.onConfirm = function() {
        if (confirmCallback) {
          confirmCallback();
        }
        close();
      }
      $scope.onCancel = function(mode) {
        if (cancelCallback) {
          cancelCallback(mode);
        }
        close();
      }

      function close() {
        $scope.element.detach();
        $scope.$destroy();
      }
    }

    function ConfirmCancelControl($scope, confirmCallback, cancelCallback) {
      CommonControl.call(this, $scope, confirmCallback, cancelCallback);

      this.setTipoTexto = function(tipoTexto) {
        $scope.tipoItem = tipoTexto;
        $scope.mensagem = `Confimar a exclusão do ${tipoTexto}`;
        return this;
      }

      this.setItem = function(item) {
        $scope.item = item;
        return this;
      }
    }

    function ProdutoSearchControl($scope, confirmCallback, cancelCallback, selectedItems) {
      function selectedData() {
        let items = selectedItems.getSelected();
        //Nesse caso é um single select
        confirmCallback(items[0]);
      }

      CommonControl.call(this, $scope, selectedData, cancelCallback);
      $scope.mensagem = "Selecione um produto abaixo";
    }

    function _getCommonTemplate() {
      return $templateCache.get('common-panel.html');
    }

    function _getSpecificTemplate(template) {
      return $templateCache.get(template);
    }

    function _makeFullTemplate(common, specific) {
      return common.replace("#MARK", specific);
    }

    function _compile(template) {
      let element = angular.element(template);
      let compiledFunction = $compile(element);
      return compiledFunction;
    }

    function _createScope() {
      return $rootScope.$new();
    }

    function _bindScope(scope, compiledFunction) {
      let elementLinked = compiledFunction(scope);
      scope.element = elementLinked;
      return elementLinked;
    }

    function _bindToBody(linkedTemplate) {
      body.append(linkedTemplate);
    }

    function _createControl(template, controlFunction, confirmCallback, cancelCallback, injectedItem) {
      let scope = _createScope();
      let confirmCancelLinked = _bindScope(scope, _compile(template));
      _bindToBody(confirmCancelLinked);
      if (!injectedItem) {
        return new controlFunction(scope, confirmCallback, cancelCallback);
      }
      return new controlFunction(scope, confirmCallback, cancelCallback, $injector.get(injectedItem));
    }

    function openConfirmCancelPanel(confirmCallback, cancelCallback) {
      let confirmCancelTemplate = _makeFullTemplate(_getCommonTemplate(), _getSpecificTemplate('confirm-cancel.html'));
      return _createControl(confirmCancelTemplate, ConfirmCancelControl, confirmCallback, cancelCallback);
    }

    function openSearchProdutoPanel(confirmCallback, cancelCallback) {
      let searchProdutoTemplate = _makeFullTemplate(_getCommonTemplate(), _getSpecificTemplate('data-search.html'));
      return _createControl(searchProdutoTemplate, ProdutoSearchControl, confirmCallback, cancelCallback, 'selectedItems');
    }

    this.openConfirmCancelPanel = openConfirmCancelPanel;
    this.openSearchProdutoPanel = openSearchProdutoPanel;

  }]);

  pdrPanels.run(['$templateRequest', '$templateCache', function($templateRequest, $templateCache){
    $templateRequest('js/services/panels/confirm-cancel/confirm-cancel.html').then(function(response) {
      $templateCache.put('confirm-cancel.html', response);
    });
    $templateRequest('js/services/panels/common-panel/common-panel.html').then(function(response) {
      $templateCache.put('common-panel.html', response);
    });
    $templateRequest('js/services/panels/data-search/data-search.html').then(function(response) {
      $templateCache.put('data-search.html', response);
    });
  }]);


})();
