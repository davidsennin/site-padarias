(function() {

  let searchPanel = angular.module('fmSearchPanel', []);

  searchPanel.directive('fmSearchPanel', ['DataProviderService', 'searchPanel', 'normalizeParam',
  function(DataProviderService, searchPanel, normalizeParam) {

    let search = {};

    search.restrict = 'A';

    search.scope = {
      service: '@',
      data: '=',
      fmSearchCallback: '='
    }

    search.compile = function(tElement, tAttr) {
      let dataService;

      return {
        pre: function(scope, iElement, iAttr) {
          if (!DataProviderService.getFunction(scope.service, "getData")) {
            throw new Error("Deve ser informado um data service válido para o search");
          }
          function refreshItems(newItem) {
            newItem = normalizeParam(newItem);
            scope.items.push(newItem);
          }
          dataService = DataProviderService.getService(scope.service);
          scope.items = dataService.safeRun("getData");
          dataService.safeRun("addListener", refreshItems);

          searchPanel.create(iElement, scope);
        },
        post: function(scope, iElement, iAttr) {
          scope.$watch('data', function(newV, oldV) {
            searchPanel.filter(newV);
          });

          let panelFocus = false;
          iElement.on('keydown', function(event) {
            if(event.key=="ArrowDown") {
              panelFocus = searchPanel.canFocus();
              searchPanel.focus();
            }
          });
          iElement.on('blur', function(event) {
            if (!panelFocus) {
              searchPanel.hide();
            }
            panelFocus = false;
          });
        }
      }
    }

    return search;

  }]);

  searchPanel.factory('searchPanel', ['$compile', 'filterFilter', function($compile, filterFilter) {

    let searchPanel = {};
    let css = {
      position: 'relative',
      left: '0',
      width: '100%',
      top: '0',
      display: 'none',
      backgroundColor: 'rgb(250, 250, 250)',
      borderRadius: '10px',
      height: '2em',
      padding: '0 0.8em'
    };

    let html = `<select tabindex="99" ng-model="data1" ng-options="item.nome for item in filteredItems"></select>`;
    let elementScope;
    let element;

    function setEvents() {
      element.on('keydown', function(event) {
        if (event.key == "Enter") {
          if (elementScope.fmSearchCallback) {
            console.log(elementScope.data1);
            elementScope.fmSearchCallback(elementScope.data1);
          }
          elementScope.$emit('padaria', 'foca-nome');
          event.preventDefault();
        }
      });
    }

    searchPanel.create = function(father, scope) {
      elementScope = scope;
      elementScope.filteredItems = [];
      let create = $compile(angular.element(html));
      element = create(scope);
      element.css(css);
      father.after(element);
      setEvents();
    }

    function hide() {
      element.css({display: 'none'});
    }
    function show() {
      element.css({display: 'inline-block'});
    }
    function setSize(qtdElements) {
      element[0].size = qtdElements+1;
      let height = 2 + qtdElements;
      if (qtdElements > 4) {
        height = 6;
      }
      element.css({height: `${height}em`});
    }
    searchPanel.hide = function() {
      hide();
    }
    searchPanel.filter = function(newText) {
      if (!newText) {
        hide();
        return;
      }
      elementScope.filteredItems = filterFilter(elementScope.items, newText);
      if (elementScope.filteredItems.length > 0) {
        show();
        setSize(elementScope.filteredItems.length);
      }
    }

    searchPanel.canFocus = function() {
      if (elementScope.filteredItems == 0) {
        return false;
      }
      return true;
    }
    searchPanel.focus = function() {
      element[0].focus();
    }
    return searchPanel;

  }]);

})();
