(function() {

  let vendas = angular.module('padarias');

  vendas.controller('VendasController', ['$scope', 'VendasDataService', 'commonLancamentoController',
  function($scope, VendasDataService, commonLancamentoController) {

    let superControl = new commonLancamentoController($scope, VendasDataService, "form_venda");
    $scope.newData = function() {
      $scope.itemVenda = {};
    }
    $scope.newData();

    $scope.receiveEditItem = function(item) {
      $scope.itemVenda = item;
    }

    $scope.create = function() {
      VendasDataService.criaVenda($scope.itemVenda);
    }

    $scope.addItem = function() {
      superControl.createData();
    }

    $scope.cancelClick = function() {
      superControl.cancel();
    }

    $scope.setProdutoSearch = function(produto) {
      $scope.itemVenda.produto = produto;
    }
  }]);

})();
