angular.module('padarias')
.controller('ProdutosController', ['$scope', 'ProdutosDataService', 'commonController',
function($scope, ProdutosDataService, commonController) {


  let superControl = new commonController($scope, ProdutosDataService, "form_produto");
  $scope.newData = function() {
    $scope.produto = {nome:undefined, custo:undefined, valor:undefined};
  }
  $scope.newData();

  $scope.receiveEditItem = function(produto) {
    $scope.produto = produto;
  }

  $scope.create = function() {
    ProdutosDataService.criaProduto($scope.produto);
  }

  $scope.addProduto = function() {
    superControl.createData();
  }

  $scope.cancelClick = function() {
    superControl.cancel();
  }

  $scope.nomeBlur = function(valor) {
    $scope.dataAdded = false;

    // Faz a busca pelo produto apenas no caso de não estar editando um produto setado pela tabela
    if (!superControl.isEditMode()) {
      let produto = ProdutosDataService.getProdutoByNome(valor);
      if (produto) {
        $scope.produto = produto;
      } else
      // Caso não esteja no modo edição e tenha algum id no produto, isso quer dizer que algum produto
      // Foi carregado previamente e deve ser limpado agora que o campo foi deixado com um nome diferente
      // Caso seja desejado uma alteração do nome, então deve ser usado o mecanismo da tabela
      if ($scope.produto._id) {
        $scope.produto._id = undefined;
      }
    }
  }

  $scope.searchCallback = function(result) {
    $scope.produto.nome = result.nome;
    $scope.$digest();
  }

}]);
