(function() {

  let module = angular.module('padarias');

  module.controller('LancamentosEstoqueController', ['$scope', 'EstoqueDataService', 'commonLancamentoController',
  function($scope, EstoqueDataService, commonLancamentoController) {

    let superControl = new commonLancamentoController($scope, EstoqueDataService, "form_estoque");
    $scope.newData = function() {
      $scope.itemEstoque = {};
    }
    $scope.newData();

    $scope.receiveEditItem = function(item) {
      $scope.itemEstoque = item;
    }

    $scope.create = function() {
      EstoqueDataService.criaItem($scope.itemEstoque);
    }

    $scope.addItem = function() {
      superControl.createData();
    }

    $scope.cancelClick = function() {
      superControl.cancel();
    }

    $scope.setProdutoSearch = function(produto) {
      $scope.itemEstoque.produto = produto;
    }

  }]);

})();
